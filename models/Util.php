<?php

namespace app\models;

use app\models\miners\Innosilicon;
use app\models\miners\PinIdea;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Util
 * @package app\models
 */
class Util
{

    public $errors = array();
    /**
     * @var Cgminer
     */
    private $miner_soft;
    private $miner;

    public function __construct($miner)
    {
        $this->miner = $miner;
        $this->switchMinerSoftware();
    }

    /**
     * Switch between miner types
     */
    private function switchMinerSoftware()
    {

        if (!empty($this->miner['model']['name'])) {
            if (strpos($this->miner['model']['name'], 'Innosilicon') !== false) {
                $this->miner_soft = new Innosilicon($this->miner['ip'], $this->miner['port'], $this->miner['model']['name']);
            } elseif (strpos($this->miner['model']['name'], 'PinIdea') !== false) {
                $this->miner_soft = new PinIdea($this->miner['ip'], $this->miner['port'], $this->miner['model']['name']);
            } else {
                $this->miner_soft = new Cgminer($this->miner['ip'], $this->miner['port']);
            }
        } else {
            $this->miner_soft = new Cgminer($this->miner['ip'], $this->miner['port']);
        }

        if (!$this->miner['port']) {
            $this->miner['port'] = 4028;
        }

    }

    /**
     * @param $s
     * @return string
     */
    public static function formatElapsedTime($s): string
    {
        if (!$s) {
            return '';
        }
        $days = floor($s / 86400);
        $left_time = $s - $days * 86400;

        $hours = floor($left_time / 3600);
        $left_time -= $hours * 3600;

        $minutes = floor($left_time / 60);
        $left_time -= $minutes * 60;

        $seconds = $left_time;

        $result = '';
        if ($days) {
            $result .= $days . Yii::t('app', 'd') . ' ';
        }
        if ($hours || $result) {
            $result .= $hours . Yii::t('app', 'h') . ' ';
        }
        if ($minutes || $result) {
            $result .= $minutes . Yii::t('app', 'm') . ' ';
        }
        $result .= $seconds . Yii::t('app', 's');

        return $result;
    }

    /**
     * @return mixed
     */
    public function getMiner()
    {
        return $this->miner;
    }

    /**
     * @param $poolId
     * @return bool
     */
    public function selectPool($poolId): bool
    {
        if ($this->isOnline($this->miner['ip'], $this->miner['port']) && $this->miner_soft->selectPool($poolId)) {
            return $this->miner_soft->reboot();
        }
        return false;
    }

    /**
     * Check if the miner is running
     *
     * @param $ip
     * @param $port
     * @return bool
     */
    public function isOnline($ip, $port): bool
    {
        if (!($fp = @fsockopen($ip, $port, $errno, $errstr, 2))) {
            return false;
        }
        if (is_resource($fp)) {
            fclose($fp);
        }
        return true;
    }

    /**
     * @param $priority
     * @return bool
     */
    public function changePoolPriority($priority): bool
    {
        if ($this->isOnline($this->miner['ip'], $this->miner['port'])) {
            return $this->miner_soft->changePoolPriority($priority);
        }
        return false;
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if ($this->isOnline($this->miner['ip'], $this->miner['port'])) {
            return $this->miner_soft->save();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function checkPrivileged(): bool
    {
        if ($this->isOnline($this->miner['ip'], $this->miner['port'])) {
            return $this->miner_soft->checkPrivileged();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function restart(): bool
    {
        return $this->miner_soft->reboot();
    }
}
