<?php

namespace app\models;


/**
 * Cgminer is the model for cgminer miners interaction.
 */
class Cgminer
{
    /**
     * @var string
     */
    protected $ip;
    /**
     * @var int
     */
    protected $port;

    public function __construct($ip, $port)
    {
        $this->ip = $ip;
        $this->port = $port;
    }

    /**
     * @param null $ip
     * @param null $port
     * @return resource|null
     */
    protected function getsock($ip = null, $port = null)
    {
        $ip = $ip ?? $this->ip;
        $port = $port ?? $this->port;
        $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        if ($socket === false || $socket === null) {
            return null;
        }

        $res = @socket_connect($socket, $ip, $port);

        if ($res === false) {
            socket_close($socket);
            return null;
        }
        return $socket;
    }

    /**
     * @param $socket
     * @return string
     */
    protected function readsockline($socket): string
    {
        $line = '';
        socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, array('sec' => 6, 'usec' => 0));
        while (true) {
            $byte = @socket_read($socket, 1);
            if ($byte === false || $byte === '') {
                break;
            }
            if ($byte === "\0") {
                break;
            }
            $line .= $byte;
        }
        return $line;
    }

    /**
     * @param string $cmd
     * @return array|mixed
     */
    protected function callMinerd($cmd = '')
    {
        if (!$cmd) {
            $cmd = '{"command":"summary+devs+pools"}';
        }

        $socket = $this->getsock();
        if ($socket !== null) {
            socket_write($socket, $cmd, strlen($cmd));
            $line = $this->readsockline($socket);
            socket_close($socket);
            if ($line === '') {
                return array('error' => true, 'code' => 'firmware_error');
            }

            // Fixed invalid JSON
            $strBug = '"}{"';
            if (strpos($line, $strBug)) {
                $line = str_replace($strBug, '"},{"', $line);
            }
            if (strpos($line, '{') === 0) {
                return json_decode($line);
            }
        }

        return array('error' => true, 'code' => 'firmware_error');
    }

    /**
     * @param $poolId
     * @return bool
     */
    public function selectPool($poolId): bool
    {
        $o = $this->callMinerd('{"command":"switchpool", "parameter":' . (int)$poolId . '}');
        return $o && !empty($o->STATUS[0]) && $o->STATUS[0]->STATUS === 'S';
    }

    /**
     * @param $priority
     * @return bool
     */
    public function changePoolPriority($priority): bool
    {
        $o = $this->callMinerd('{"command":"poolpriority", "parameter":"' . $priority . '"}');
        return $o && !empty($o->STATUS[0]) && $o->STATUS[0]->STATUS === 'S';

    }

    /**
     * @return bool
     */
    public function checkPrivileged(): bool
    {
        $o = $this->callMinerd('{"command":"privileged"}');
        return $o && !empty($o->STATUS[0]) && $o->STATUS[0]->STATUS === 'S';
    }

    /**
     * @return bool
     */
    public function reboot(): bool
    {
        try {
            ini_set('default_socket_timeout', 2);
            if ($ssh = @ssh2_connect($this->ip)) {
                ssh2_auth_password($ssh, 'root', 'admin');
                $stream = ssh2_exec($ssh, '/sbin/reboot');
                $errorStream = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
                stream_set_blocking($errorStream, true);
                stream_set_blocking($stream, true);
                $error = stream_get_contents($errorStream);
                if (!$error) {
                    return true;
                }
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
}
