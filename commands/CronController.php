<?php

namespace app\commands;

use app\commands\workers\CommonWorker;
use app\daemons\Pusher;
use app\models\miner\Miner;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\Wamp\WampServer;
use Ratchet\WebSocket\WsServer;
use React\EventLoop\Factory;
use React\Socket\Server;
use React\ZMQ\Context;
use yii\console\Controller;
use ZMQ;
use ZMQContext;

/**
 * Class CronController
 * @package app\commands
 */
class CronController extends Controller
{

    /**
     * Start Ratchet server
     */
    public function actionServerStart(): void
    {
        $loop = Factory::create();
        $pusher = new Pusher();
        // Listen for the web server to make a ZeroMQ push after an ajax request
        $context = new Context($loop);
        $pull = $context->getSocket(ZMQ::SOCKET_PULL);
        $pull->bind('tcp://127.0.0.1:5555'); // Binding to 127.0.0.1 means the only client that can connect is itself
        $pull->on('message', array($pusher, 'onUpdate'));

        // Set up our WebSocket server for clients wanting real-time updates
        $webSock = new Server('0.0.0.0:8080', $loop); // Binding to 0.0.0.0 means remotes can connect
        $webServer = new IoServer(
            new HttpServer(
                new WsServer(
                    new WampServer(
                        $pusher
                    )
                )
            ),
            $webSock
        );
        $loop->run();
    }

    /**
     * @param String $command
     * @param array $params
     * @return bool
     * @throws \ZMQSocketException
     */
    public static function sendWebsockCommand(String $command, $params = array()): bool
    {
        $context = new ZMQContext();
        $socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'cron pusher');
        $socket->setSockOpt(ZMQ::SOCKOPT_LINGER, 500);
        $socket->setSockOpt(ZMQ::SOCKOPT_SNDTIMEO, 500);
        $socket->setSockOpt(ZMQ::SOCKOPT_RCVTIMEO, 500);
        if (!$socket->connect('tcp://127.0.0.1:5555')) {
            return false;
        }
        $socket->send(json_encode(['command' => $command, 'params' => $params]));
        return true;
    }

    /**
     * Are miners editable?
     */
    public function actionCheckPrivileged(): void
    {
        $miners = Miner::find()->with('model')->groupBy('ip')->asArray()->all();
        $pool = new \Pool(500, CommonWorker::class);
        foreach ($miners as $key => $miner) {
            $pool->submit(new CheckPrivilegedThread($miner));
        }
        while ($pool->collect()) {
            continue;
        }
        $pool->shutdown();
    }

    /**
     * Clear old data
     */
    public function actionClear(): void
    {
        Clear::clearCommands();
        Clear::clearPoolInfo();
        Clear::clearViewPoolInfo();
        Clear::clearNotification();
        Clear::clearLoginAttempts();
        Clear::clearBannedIp();
    }
}