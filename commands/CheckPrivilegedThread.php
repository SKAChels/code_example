<?php

namespace app\commands;

use app\models\miner\Miner;
use app\models\Util;
use yii\base\InvalidConfigException;

class CheckPrivilegedThread extends \Thread
{
    private $miner;

    public function __construct($miner)
    {
        $this->miner = (array)$miner;
    }

    public function run()
    {
        defined('YII_DEBUG') or define('YII_DEBUG', true);
        defined('STDIN') or define('STDIN', fopen('php://stdin', 'b'));
        defined('STDOUT') or define('STDOUT', fopen('php://stdout', 'b'));
        require_once __DIR__ . '/../vendor/autoload.php';
        require_once __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
        $config = require __DIR__ . '/../config/console.php';
        try {
            new \yii\console\Application($config);
        } catch (InvalidConfigException $e) {
            return;
        }

        $util = new Util($this->miner);
        $privileged = $util->checkPrivileged() ? 1 : 0;
        $miner = Miner::findOne(['id' => $this->miner['id']]);
        if ($miner === null) {
            return;
        }
        if ($miner->privileged !== $privileged) {
            $miner->privileged = $privileged;
            $miner->save();
        }
    }
}